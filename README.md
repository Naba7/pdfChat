# PdfChat


## Getting started

You can chat with any PDF file of your choice. Ask questions or learn about the summary/context of the entire PDF file.

## Install and run this app on your system

Follow the steps to make this app run on localhost on your Linux/MacOS/Windows system.

```
git clone https://gitlab.com/Naba7/pdfChat.git

cd pdfChat

virtualenv pdfChat

source pdfChat/bin/activate
```

Then create a ``.env`` file with contents as ``OPENAI_API_KEY="$"``. Add your own OpenAI API Key instead of ``$``. After this, follow the next steps.

```
pip install -r requirements.txt

streamlit run app.py

```
