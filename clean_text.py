import fitz
import os
import re
import numpy as np

filename = "bridgewater_treatise.pdf"
doc = fitz.open(filename) # open a document


#bool(re.search(r"^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$", string))  

counter = 1
out = open("output.txt", "wb") # create a text output
for page in doc: # iterate the document pages
    text = page.get_text().encode("utf8") # get plain text (is in UTF-8)
    out.write(text) # write text of page
    out.write("\n\n".encode())
	# out.write(bytes((12,))) # write page delimiter (form feed 0x0C)
out.close


regex = "^[A-Z]+$"
remove_words = ["chap.", "Digitized by Google", "Digitized by Coogle", "Digitized by Go", "Digitized by GoogI", "ogI", "Digitized by Googl", "Digitizad by Google", ". .  . . ", "4.  . .", " . 8.  . . ", "\ FAVOUR OF DESIGN*", "\*", "\• ", "\•", "\• "]
#page_markers = ["Digitized by Google", "Digitized by Coogle", "Digitized by Go", "Digitized by GoogI", "ogI", "Digitized by Googl"]

pattern = re.compile("^(M{0,3})(C(?:D|M)|D?C{0,3})(X(?:L|C)|L?X{0,3})(I(?:V|X)|V?I{0,3})(i(?:v|x)|v?i{0,3}).$")


pages = open("output.txt").read().split("\n\n")
han = open("modi-output.txt", "wb")
for page in pages:
      
    for word in remove_words:
        page = re.sub(word, "", page)
    num = re.findall("[0-9]*", page)
    for n in num:
        page = re.sub(n, "", page)


    rom = re.findall(pattern, page)
    for r in rom:
        page = re.sub(rom, "", page)

    sentences = page.split("\n")
    sentence_arr = []
    for sentence in sentences:
        if not sentence.isupper():
            sentence_arr.append(sentence)
    page = (" ").join(sentence_arr)
    
   

    page = re.sub(r'([a-z])- ([a-z])', r'\1\2', page)
    page = re.sub(r'([a-z])-  ([a-z])', r'\1\2', page)
    page = re.sub(r'([a-z])-   ([a-z])', r'\1\2', page)
    page = re.sub(r'([a-z])-   ([a-z])', r'\1\2', page)
    page = re.sub(r"([a-z])- ' ([a-z])", r'\1\2', page)
    page = re.sub(r"([a-z])- “ ([a-z])", r'\1\2', page)
    page = re.sub(r"([a-z]) “ ([a-z])", r'\1\2', page)
    page = re.sub(r"([a-z])- • ([a-z])", r'\1\2', page)


    mix = ["See Note D in Appendix. ", "in the Appendix, Note B.   ", "See Note A in the Appendix.", "See Note C in the Appendix. ", "See First Lecture on Political Economy.", "See Note C in the Appendix."]
    for word in mix:
        page = re.sub(word, "", page)

  
    han.write(page.encode())
han.close()


# sentences = open("modi-output.txt").read().split(".")
# total = len(sentences)
# #print(total)
# para = sentences[1] + ". "
# # for i in range(2, total):
# #     print(sentences[i] + str(len(sentences[i])))
# #     print("\n\n")
# para_arr = []
# counter = 1
# for i in range(2, total):
#     if counter < 2:
#         if len(sentences[i].split()) > 30 and len(sentences[i].split()) < 55:
#             para = para + sentences[i]
#             para = para + ". "
#             counter = counter + 1
#     else:
#         para_arr.append(para)
#         para = ""
#         counter = 1
#     if i == total - 1 and counter < 2:
#         para_arr.append(para)
#         para = ""
#         counter = 1

# np.savetxt('final-output.txt', para_arr, delimiter =', ', fmt='%s') 

    




