import streamlit as st
import os
import re
import numpy as np
from dotenv import load_dotenv
import pickle
from PyPDF2 import PdfReader
from streamlit_extras.add_vertical_space import add_vertical_space
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import FAISS
from langchain.llms import OpenAI
from langchain.chains.question_answering import load_qa_chain
from langchain.chains import RetrievalQA
from langchain.callbacks import get_openai_callback
from langchain.prompts import PromptTemplate


from spellchecker import SpellChecker
import wordninja



load_dotenv()
last_response = {}
spell = SpellChecker()


col1, col2 = st.columns(2, gap="large")

with col1:
    st.header("Variables")

    st.text("**Everything is exposed.**")
    per_sim = st.select_slider(
        'Select the percentage similarity you want from quotations(0 being most similar and 1 being least similar):',
        options=[i/10 for i in range(11)])
   
  
    number = st.select_slider(
        'Select the range of sentences:',
        options=[i for i in range(11)])
    

    option = st.selectbox(
        'How would you like to be prompted?',
        ('Quotation', 'Hallucination', 'Best of both'))
    
    filter = st.selectbox(
        'Select the filter you want to apply',
        ('None', 'Avoid mentioning God', 'No slang words', 'Be humble', 'No inappropriate response'))
    
    ownFilter = st.text_input('You can add your own filter: ')


    
# Text cleaning
sentences = open("modi-output.txt").read().split(".")
total = len(sentences)
para = sentences[1] + ". "
para_arr = []
c = 1
number = int(number)
for i in range(2, total):
    if c < 2:
        if len(sentences[i].split()) > number*10 and len(sentences[i].split()) < (number+1)*10:
            para = para + sentences[i]
            para = para + ". "
            c = c + 1
    else:
        para_arr.append(para)
        para = ""
        c = 1
    if i == total - 1 and c < 2:
        para_arr.append(para)
        para = ""
        c = 1

np.savetxt('final-output.txt', para_arr, delimiter =', ', fmt='%s')
texts = []
file = open("final-output.txt", "r")
total_lines = file.readlines()

count = 0
# Strips the newline character
for line in total_lines:
    #print(line)
    count += 1
    texts.append(line)

texts = np.asarray(texts).ravel()




def main():
    with col2:
        st.header("Chat with Charles Babbage")
        embeddings = OpenAIEmbeddings()
        VectorStore = FAISS.from_texts(texts, embedding=embeddings)
        
        
        # Accept user question/query
        query = st.text_input("Ask questions about your PDF file:")
        global counter
        global response_set
        global response
        if query:
            
            # Sentence matching prompt
            template = """Always use the source data file {context} to reply to {question} at the end.
            Add spaces between the words as necessary and stop after you find the first full stop.
            Query: {question}
            Reply in first person and in English and limit your reply to 1 sentence or 20 words atmax:"""

            PROMPT = PromptTemplate(
                input_variables=["question", "context"], template=template,
            )

            chain_type_kwargs = {"prompt": PROMPT}
            print(per_sim)
            vectordbkwargs = {"search_distance": per_sim} # predictions be more random, then higher the temperature
            qa = RetrievalQA.from_chain_type(llm=OpenAI(), chain_type="stuff", retriever=VectorStore.as_retriever(search_type='similarity'), chain_type_kwargs=chain_type_kwargs, return_source_documents=True, verbose=False) # keep verbose=True during dev stage
            result = qa({'query': query, 'vectordbkwargs': vectordbkwargs})
            
            
            # print(result)



            if option == 'Quotation':
                response = result["source_documents"][0].page_content
            elif option == 'Hallucination':
                response = result["result"]
            else:
                response = result["result"] + "\n\n" + "A quotation from my works:  " + result["source_documents"][1].page_content

            if option != 'Hallucination':
                if response in response_set:
                        counter = counter + 1
                        response = result["source_documents"][counter].page_content
                else:
                        response_set.add(response)

                if counter == 4:
                        counter = 0
                    
                if len(response_set) == 4:
                        response_set = set()

            
                try:
                    # print(len(response))
                    if len(response) > 10:
                        sentence = re.findall(r"[\w]+|[^\s\w]", response)
                        # find those words that may be misspelled
                        misspelled = spell.unknown(sentence)
                        sentence_arr = []

                        for check_word in sentence:
                            if check_word in misspelled:
                                # Get the one `most likely` answer
                                if spell.correction(check_word) is None:
                                    split_words = wordninja.split(check_word)
                                    for split_word in split_words:
                                        sentence_arr.append(spell.correction(split_word))
                                else:
                                    sentence_arr.append(spell.correction(check_word))

                            else:
                                sentence_arr.append(check_word)

                        #print(sentence_arr)
                        text = " ".join(sent for sent in sentence_arr)
                    

                        final = re.sub(r'\s([?.!,;:"](?:\s|$))', r'\1', text)
                  
                        final = final[0].upper() + final[1:]
                        response  = final

                    else:
                        response = None
                except:
                    raise Exception("Response is generated but is not sent back to the frontend! Please lower down frequency of sending query/question to backend!")
                
            # print(response)
            # docs = VectorStore.similarity_search(query=query, k=3)
            # template = """Search the internet to learn about the writing style and tone use by 
            #     Charles Babbage, 19th century mathematician, philosopher, inventor and 
            #     mechanical engineer and then reply to {question} in the similar 19th century 
            #     writing style and tone. Now act and reply like the 19th century Charles Babbage.
            #     {context}

            #     Question: {question}

            #     Reply in 2 sentences atmax and only in the tone of 19th century mathematician, 
            #     philosopher, inventor and mechanical engineer Charles Babbage and only to 
            #     topics or text that you think Charles Babbage would know. If the topics or 
            #     text that got introduced after Babbage's death, i.e. 1871, reply in a way that 
            #     shows you aren't aware of topic(s).
                
            #     Do not use inappropriate words or responses."""
            # PROMPT = PromptTemplate(
            #     input_variables=["question", "context"], template=template,
            # )
            # #chain_type_kwargs = {"prompt": PROMPT}

            # llm = OpenAI(model="text-davinci-003", temperature=0) # model_name="gpt-3.5-turbo"
            # vectordbkwargs = {"search_distance": per_sim} # predictions be more random, then higher the temperature
            # #chain = RetrievalQA.from_chain_type(llm=llm, chain_type="stuff", retriever=docs, chain_type_kwargs=chain_type_kwargs, return_source_documents=True, verbose=False) # keep verbose=True during dev stage

            # chain = load_qa_chain(llm=llm, chain_type="stuff", prompt=PROMPT, verbose=False)
            # with get_openai_callback() as cb:
            #     #response = chain({'query': query, 'vectordbkwargs': vectordbkwargs})
            #     response = chain.run(input_documents=docs, question=query)
            #     print(cb)
            st.write("The hallucinated text:    ", result["result"])
            for i in range(4):
                st.write("The quotation " + str(i+1) + " from Babbage's works:    ", result["source_documents"][i].page_content)
            # st.write(response)
        



if __name__=='__main__':
    response_set = set()
    counter = 0
    response = ""
    main()

